"""dj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .views import (CityHomeListView, CityDetailView, CityCreateView, CityUpdateView,
                    CityDeleteView, TrainHomeListView, TrainDetailView, TrainUpdateView,
                    TrainDeleteView, TrainCreateView, RouteHomeView, RouteAddView,
                    RouteListView, RouteDetailView, RouteDeleteView, LoginView,
                    LogoutView, RegisterView)

urlpatterns = [
    path('cities/', CityHomeListView.as_view(), name='cities_home'),
    path('cities/detail/<int:pk>/', CityDetailView.as_view(), name='city_detail'),
    path('cities/update/<int:pk>/', CityUpdateView.as_view(), name='city_update'),
    path('cities/delete/<int:pk>/', CityDeleteView.as_view(), name='city_delete'),
    path('cities/add/', CityCreateView.as_view(), name='city_add'),
    path('trains/', TrainHomeListView.as_view(), name='trains_home'),
    path('trains/detail/<int:pk>/', TrainDetailView.as_view(), name='train_detail'),
    path('trains/update/<int:pk>/', TrainUpdateView.as_view(), name='train_update'),
    path('trains/delete/<int:pk>/', TrainDeleteView.as_view(), name='train_delete'),
    path('trains/add/', TrainCreateView.as_view(), name='train_add'),
    path('', RouteHomeView.as_view(), name='home'),
    path('add_route/', RouteAddView.as_view(), name='add_route'),
    path('route_list/', RouteListView.as_view(), name='route_list'),
    path('detail/<int:pk>/', RouteDetailView.as_view(), name='route_detail'),
    path('delete/<int:pk>/', RouteDeleteView.as_view(), name='route_delete'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
]
