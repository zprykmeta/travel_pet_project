from django.contrib import messages
from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, logout, login
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import (
    DetailView as DV,
    ListView as LV,
    CreateView as CV,
    UpdateView as UV,
    DeleteView as DelV,
    FormView as FV,
    RedirectView as RV
)
from .utils import dfs_paths, get_graph
from .models import Train, City, Route
from .forms import RouteForm, RouteModelForm, CityForm, TrainForm, UserLoginForm, UserRegistrationForm


class LoginView(FV):
    template_name = 'login.html'
    form_class = UserLoginForm
    success_url = '/'

    def form_valid(self, form):
        username = self.request.POST['username']
        password = self.request.POST['password']
        user = authenticate(username=username, password=password)
        login(self.request, user)

        return super(LoginView, self).form_valid(form)


class LogoutView(RV):
    url = '/'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class RegisterView(SuccessMessageMixin, FV):
    template_name = 'register.html'
    form_class = UserRegistrationForm
    success_url = reverse_lazy('login')
    success_message = 'Register done! Now u can login!'

    def form_valid(self, form):
        new_user = form.save(commit=False)
        new_user.set_password(form.cleaned_data['password'])
        new_user.save()
        return super(RegisterView, self).form_valid(form)


class RouteHomeView(FV):
    form_class = RouteForm
    success_url = reverse_lazy('home')
    template_name = 'home.html'

    def form_valid(self, form):
        data = form.cleaned_data
        from_city = data['from_city']
        to_city = data['to_city']
        across_cities_form = data['across_cities']
        travel_time = data['travel_time']
        all_ways = list(
            dfs_paths(get_graph(), from_city.id, to_city.id))

        if len(all_ways) == 0:
            messages.error(self.request, 'Route not found!')
            return render(self.request, 'home.html', {'form': form})

        if across_cities_form:
            across_cities = [city.id for city in across_cities_form]
            good_ways = []

            for way in all_ways:
                if all(point in way for point in across_cities):
                    good_ways.append(way)

            if not good_ways:
                messages.error(self.request, 'Unreal route!')
                return render(self.request, 'home.html', {'form': form})

        trains = []

        for route in all_ways:
            tmp = {'trains': [], 'total_time': 0}

            for index in range(len(route)-1):
                queryset = Train.objects.filter(
                    from_city=route[index], to_city=route[index+1]).order_by('travel_time').first()
                tmp['total_time'] += queryset.travel_time
                tmp['trains'].append(queryset)

            if tmp['total_time'] <= travel_time:
                trains.append(tmp)

            if not trains:
                messages.error(self.request, 'Travel time more then input!')
                return render(self.request, 'home.html', {'form': form})

        routes = []

        for tr in trains:
            routes.append({'route': tr['trains'], 'total_time': tr['total_time'],
                           'from_city': from_city.name, 'to_city': to_city.name})

        sorted_routes = []

        if len(routes) == 1:
            sorted_routes = routes
        else:
            times = sorted(list(set(i['total_time'] for i in routes)))

            for time in times:
                for route in routes:
                    if time == route['total_time']:
                        sorted_routes.append(route)

        context = {'form': form, 'routes': sorted_routes, 'cities': {
            'from_city': from_city.name, 'to_city': to_city.name}}

        return render(self.request, 'home.html', context)


class RouteAddView(SuccessMessageMixin, FV):
    form_class = RouteModelForm
    success_url = reverse_lazy('home')
    template_name = 'route_create.html'
    success_message = 'Route saved!'

    def form_valid(self, form):
        data = form.cleaned_data
        name = data['name']
        from_city = data['from_city']
        to_city = data['to_city']
        across_cities = data['across_cities'].split(' ')
        trains = [int(i) for i in across_cities if i.isalnum()]
        travel_time = data['travel_time']
        queryset = Train.objects.filter(id__in=trains)
        route = Route(name=name, from_city=from_city,
                      to_city=to_city, travel_time=travel_time)
        route.save()

        for tr in queryset:
            route.across_cities.add(tr.id)

        return redirect('route_list')

    def get(self, request):
        data = request.GET

        if data:
            from_city = data['from_city']
            to_city = data['to_city']
            across_cities = data['across_cities'].split(' ')
            trains = [int(i) for i in across_cities if i.isalnum()]
            travel_time = data['travel_time']
            queryset = Train.objects.filter(id__in=trains)
            train_list = ' '.join(str(i) for i in trains)
            form = RouteModelForm(initial={'from_city': from_city, 'to_city': to_city,
                                           'across_cities': train_list, 'travel_time': travel_time})
            route_description = []

            for tr in queryset:
                description = f'Train {tr.name} from {tr.from_city} to {tr.to_city}. Travel time - {tr.travel_time}'
                route_description.append(description)

            context = {'form': form, 'description': route_description,
                       'from_city': from_city, 'to_city': to_city, 'travel_time': travel_time}

            return render(request, 'route_create.html', context)
        else:
            messages.error(request, 'Unreal to save, route does not exist!')
            return redirect('/')


class RouteListView(LV):
    model = Route
    queryset = model.objects.all()
    context_object_name = 'objects_list'
    template_name = 'route_list.html'
    paginate_by = 5


class RouteDetailView(DV):
    model = Route
    queryset = model.objects.all()
    context_object_name = 'object'
    template_name = 'route_detail.html'


class RouteDeleteView(LoginRequiredMixin, DelV):
    model = Route
    success_url = reverse_lazy('home')
    login_url = '/login/'

    def get(self, request, *args, **kwargs):
        messages.success(request, 'Route delete done!')
        return self.post(request, *args, **kwargs)


class CityHomeListView(LV):
    model = City
    queryset = model.objects.all()
    context_object_name = 'objects_list'
    template_name = 'cities_home.html'
    paginate_by = 2


class CityDetailView(DV):
    model = City
    queryset = model.objects.all()
    context_object_name = 'object'
    template_name = 'city_detail.html'


class CityCreateView(SuccessMessageMixin, LoginRequiredMixin, CV):
    model = City
    login_url = '/login/'
    form_class = CityForm
    template_name = 'city_create.html'
    success_url = reverse_lazy('cities_home')
    success_message = 'City create done!'


class CityUpdateView(SuccessMessageMixin, LoginRequiredMixin, UV):
    model = City
    login_url = '/login/'
    form_class = CityForm
    template_name = 'city_update.html'
    success_url = reverse_lazy('cities_home')
    success_message = 'City update done!'


class CityDeleteView(LoginRequiredMixin, DelV):
    model = City
    login_url = '/login/'
    template_name = 'city_delete.html'
    success_url = reverse_lazy('cities_home')

    def get(self, request, *args, **kwargs):
        messages.success(request, 'City delete done!')
        return self.post(request, *args, **kwargs)


class TrainHomeListView(LV):
    model = Train
    queryset = model.objects.all()
    context_object_name = 'objects_list'
    template_name = 'trains_home.html'
    paginate_by = 5


class TrainDetailView(DV):
    model = Train
    queryset = model.objects.all()
    context_object_name = 'object'
    template_name = 'train_detail.html'


class TrainCreateView(SuccessMessageMixin, LoginRequiredMixin, CV):
    model = Train
    login_url = '/login/'
    form_class = TrainForm
    template_name = 'train_create.html'
    success_url = reverse_lazy('trains_home')
    success_message = 'Train create done!'


class TrainUpdateView(SuccessMessageMixin, LoginRequiredMixin, UV):
    model = Train
    login_url = '/login/'
    form_class = TrainForm
    template_name = 'train_update.html'
    success_url = reverse_lazy('trains_home')
    success_message = 'Train update done!'


class TrainDeleteView(LoginRequiredMixin, DelV):
    model = Train
    login_url = '/login/'
    template_name = 'train_delete.html'
    success_url = reverse_lazy('trains_home')

    def get(self, request, *args, **kwargs):
        messages.success(request, 'Train delete done!')
        return self.post(request, *args, **kwargs)
