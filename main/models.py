from django.db import models
from django.core.exceptions import ValidationError


class City(models.Model):
    name = models.CharField(max_length=100, unique=True, verbose_name='City')

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'
        ordering = ['name']

    def __str__(self):
        return self.name


class Train(models.Model):
    name = models.CharField(max_length=100, unique=True, verbose_name='Train')
    from_city = models.ForeignKey(
        City, on_delete=models.CASCADE, verbose_name='From', related_name='from_city')
    to_city = models.ForeignKey(
        City, on_delete=models.CASCADE, verbose_name='To', related_name='to_city')
    travel_time = models.IntegerField(verbose_name='Travel time')

    class Meta:
        verbose_name = 'Train'
        verbose_name_plural = 'Trains'
        ordering = ['name']

    def __str__(self):
        return f'Train {self.name} from {self.from_city} to {self.to_city}'

    def clean(self, *args, **kwargs):
        if self.from_city == self.to_city:
            raise ValidationError('Change arrived city')

        queryset = Train.objects.filter(
            from_city=self.from_city, to_city=self.to_city, travel_time=self.travel_time).exclude(pk=self.pk)

        if queryset.exists():
            raise ValidationError('Change travel time')

        return super(Train, self).clean(*args, **kwargs)


class Route(models.Model):
    name = models.CharField(max_length=100, unique=True,
                            verbose_name='Route name')
    from_city = models.CharField(max_length=100, verbose_name='From')
    to_city = models.CharField(max_length=100, verbose_name='To')
    across_cities = models.ManyToManyField(
        Train, blank=True, verbose_name='Across cities')
    travel_time = models.IntegerField(verbose_name='Travel time')

    class Meta:
        verbose_name = 'Route'
        verbose_name_plural = 'Routes'
        ordering = ['name']

    def __str__(self):
        return f'Route {self.name}'
