from django import forms
from django.contrib.auth import authenticate, get_user_model
from .models import Train, City, Route

User = get_user_model()


class UserLoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)

            if not user:
                raise forms.ValidationError('User not exist!')
            if not user.check_password(password):
                raise forms.ValidationError('Password wrong!')

        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserRegistrationForm(forms.ModelForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password_check = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta(object):
        model = User
        fields = ('username', )

    def clean_password_check(self, *args, **kwargs):
        data = self.cleaned_data

        if data['password'] != data['password_check']:
            raise forms.ValidationError('Passwords do not match!')

        return data['password_check']


class TrainForm(forms.ModelForm):
    name = forms.CharField(label='Train name', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Input train name'}))
    from_city = forms.ModelChoiceField(label='From', queryset=City.objects.all(
    ), widget=forms.Select(attrs={'class': 'form-control', 'placeholder': 'Input from city'}))
    to_city = forms.ModelChoiceField(label='To', queryset=City.objects.all(
    ), widget=forms.Select(attrs={'class': 'form-control', 'placeholder': 'Input to city'}))
    travel_time = forms.IntegerField(label='Travel time', widget=forms.NumberInput(
        attrs={'class': 'form-control', 'placeholder': 'Input travel time'}))

    class Meta(object):
        model = Train
        fields = ('name', 'from_city', 'to_city', 'travel_time', )


class CityForm(forms.ModelForm):
    name = forms.CharField(label='City', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Input name'}))

    class Meta(object):
        model = City
        fields = ('name', )


class RouteForm(forms.Form):
    from_city = forms.ModelChoiceField(label='From', queryset=City.objects.all(
    ), widget=forms.Select(attrs={'class': 'form-control js-example-basic-single'}))
    to_city = forms.ModelChoiceField(label='To', queryset=City.objects.all(
    ), widget=forms.Select(attrs={'class': 'form-control js-example-basic-single'}))
    across_cities = forms.ModelMultipleChoiceField(label='Across cities', queryset=City.objects.all(
    ), required=False, widget=forms.SelectMultiple(attrs={'class': 'form-control js-example-basic-multiple'}))
    travel_time = forms.IntegerField(
        label='Expected travel time', widget=forms.NumberInput(attrs={'class': 'form-control'}))


class RouteModelForm(forms.ModelForm):
    name = forms.CharField(label='Route name', widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    from_city = forms.CharField(widget=forms.HiddenInput())
    to_city = forms.CharField(widget=forms.HiddenInput())
    across_cities = forms.CharField(widget=forms.HiddenInput())
    travel_time = forms.IntegerField(widget=forms.HiddenInput())

    class Meta:
        model = Route
        fields = ('name', 'from_city', 'to_city',
                  'across_cities', 'travel_time', )