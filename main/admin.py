from django.contrib import admin
from .models import City, Train, Route

admin.site.register(City)
admin.site.register(Train)
admin.site.register(Route)